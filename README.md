# sisop-praktikum-modul-1-2023-MH-IT15
Laporan Resmi pengerjaan soal shift modul 1 Praktikum Sistem Operasi 2023 Kelompok IT15



## Anggota Kelompok
1. Ilhan Ahmad Syafa (5027221040)
2. Subkhan Masudi (5027221044)
3. Gilang Raya Kurniawan (5027221045)

# SOAL 1

Farrel ingin membuat sebuah playlist yang sangat edgy di tahun ini. Farrel ingin playlistnya banyak disukai oleh orang-orang lain. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa data untuk membuat playlist terbaik di dunia. Untung saja Farrel menemukan file playlist.csv yang berisi top lagu pada spotify beserta genrenya.

## Problem Soal 1

A. Farrel ingin memasukan lagu yang bagus dengan genre hip hop. Oleh karena itu, Farrel perlu melakukan testi terlebih dahulu. Tampilkan 5 top teratas lagu dengan genre hip hop berdasarkan popularity.

B. Karena Farrel merasa kurang dengan lagu tadi, dia ingin mencari 5 lagu yang paling rendah diantara lagu ciptaan John Mayer berdasarkan popularity.

C. Karena Farrel takut lagunya kurang enak didengar dia ingin mencari lagu lagi, cari 10 lagu pada tahun 2004 dengan rank popularity tertinggi

D. Farrel sangat suka dengan lagu paling keren dan edgy di dunia. Lagu tersebut diciptakan oleh ibu Sri. Bantu Farrel mencari lagu tersebut dengan kata kunci nama pencipta lagu tersebut.


## Solution Soal 1 

[Source Code Soal 1](https://gitlab.com/subkhanmasudi8/sisop-praktikum-modul-1-2023-mh-it15/-/blob/main/soal_1/playlist_keren.sh?ref_type=heads)

### Solution Soal 1 A

pertama-tama kalian bisa download dulu file playlist yang sudah disediakan, lalu buat "mkdir sisop1" agar memudahkan mengelola file, setelah itu buat lagi "nano playlist_keren.sh" untuk menyimpan data yang sudah dicari, dari soal a kita disuruh untuk menampilkan 5 top teratas lagu dengan genre hip hop berdasarkan popularity

<a href="https://ibb.co/2S4rq4b"><img src="https://i.ibb.co/cwn9Jnm/top-lagu.png" alt="top-lagu" border="0"></a>

``` 


grep -i 'Hip Hop' top_lagu_milik_farrel.csv | sort -t, -k15,15nr | head -n 5 


```

<a href="https://ibb.co/myfyfwP"><img src="https://i.ibb.co/1XFXF54/Screenshot-2023-09-30-151737.png" alt="Screenshot-2023-09-30-151737" border="0"></a>

dengan memasukkan code tersebut kita bisa menampilkan 5 top tertinggi lagu dengan genre 'hip hop' berdasarkan popularitynya

perintah grep untuk mencari baris-baris dalam file top_lagu_milik_farrel.csv yang mengandung teks 'Hip Hop'. kemudian masukkan nama file sebagai pencarian yaitu top_lagu_milik_farrel.csv. kemudian '-i' membuat pencarian bersifat case-insensitive, artinya tidak memperdulikan huruf besar atau kecil. kemudian '|'Ini adalah operator pipeline, yang mengarahkan output dari perintah sebelumnya menjadi input untuk perintah berikutnya. perintah 'sort' untuk mengurutkan baris-baris berdasarkan kolom ke-15 (dengan delimiter koma, 't',). kemudian '-k15,15' menunjukkan bahwa pengurutan dilakukan berdasarkan kolom ke-15. kemudian '-nr' digunakan untuk mengurutkan secara numerik secara terbalik, sehingga baris dengan nilai kolom ke-15 yang lebih tinggi akan muncul lebih dulu. kemudian perintah 'head' untuk menampilkan 5 baris pertama dari output yang telah dihasilkan. dan '-n 5' menunjukkan jumlah baris yang akan ditampilkan.

### Solution Soal 1 B 

dari soal b kita disuruh untuk menampilkan 5 top terendah lagu ciptaan dari 'John Mayer' berdasarkan popularitynya

```


grep -i 'John Mayer' top_lagu_milik_farrel.csv | sort -t, -k15,15n | head -n 5 


```

<a href="https://ibb.co/cN4mwb8"><img src="https://i.ibb.co/brcfFHv/b.png" alt="b" border="0"></a>

dengan memasukkan code tersebut kita bisa menampilkan 5 top terendah lagu yang diciptakan dari 'John Mayer' berdasarkan popularitynya

perintah grep untuk mencari baris-baris dalam file top_lagu_milik_farrel.csv yang mengandung teks 'John Mayer'. kemudian masukkan nama file sebagai pencarian yaitu top_lagu_milik_farrel.csv. kemudian '-i' membuat pencarian bersifat case-insensitive, artinya tidak memperdulikan huruf besar atau kecil. kemudian '|'Ini adalah operator pipeline, yang mengarahkan output dari perintah sebelumnya menjadi input untuk perintah berikutnya. perintah 'sort' untuk mengurutkan baris-baris berdasarkan kolom ke-15 (dengan delimiter koma, 't',). kemudian '-k15,15' menunjukkan bahwa pengurutan dilakukan berdasarkan kolom ke-15. kemudian '-n' digunakan untuk mengurutkan secara numerik, sehingga baris dengan nilai kolom ke-15 yang lebih rendah akan dimunculkan terlebih dahulu. kemudian perintah 'head' untuk menampilkan 5 baris pertama dari output yang telah dihasilkan. dan '-n 5' menunjukkan jumlah baris yang akan ditampilkan.

### Solution Soal 1 C

dari soal c kita disuruh untuk menampilkan 10 top tertinggi lagu yang diciptakan pada tahun '2004' berdasarkan popularitynya

```

grep -i '2004' top_lagu_milik_farrel.csv | sort -t, -k15,15n | head -n 10


```

<a href="https://ibb.co/SQTYK8Y"><img src="https://i.ibb.co/B4DWgxW/vc.png" alt="vc" border="0"></a>

dengan memasukkan code tersebut kita bisa menampilkan 10 top tertinggi lagu yang diciptakan pada tahun '2004' berdasarkan popularitynya

perintah grep untuk mencari baris-baris dalam file top_lagu_milik_farrel.csv yang mengandung teks '2004'. kemudian '-i' membuat pencarian bersifat case-insensitive, artinya tidak memperdulikan huruf besar atau kecil. kemudian masukkan nama file sebagai pencarian yaitu top_lagu_milik_farrel.csv. kemudian '|'Ini adalah operator pipeline, yang mengarahkan output dari perintah sebelumnya menjadi input untuk perintah berikutnya. perintah 'sort' untuk mengurutkan baris-baris berdasarkan kolom ke-15 (dengan delimiter koma, 't',). kemudian '-k15,15' menunjukkan bahwa pengurutan dilakukan berdasarkan kolom ke-15. kemudian '-n' digunakan untuk mengurutkan secara numerik, sehingga baris dengan nilai kolom ke-15 yang lebih rendah akan dimunculkan terlebih dahulu. kemudian perintah 'head' untuk menampilkan 10 baris pertama dari output yang telah dihasilkan. dan '-n 10' menunjukkan jumlah baris yang akan ditampilkan.

### Solution Soal 1 D

dari soal d kita disuruh untuk menampilkan lagu dengan kata kunci ibu "Sri"

```

grep -i 'sri' top_lagu_milik_farrel.csv


```

<a href="https://ibb.co/VBXXcW5"><img src="https://i.ibb.co/h8ppJRw/sri.png" alt="sri" border="0"></a>

dengan memasukkan code tersebut kita bisa menampilkan lagu dengan kata kunci ibu "Sri"

perintah grep untuk mencari baris-baris dalam file top_lagu_milik_farrel.csv yang mengandung teks 'sri'. kemudian '-i' membuat pencarian bersifat case-insensitive, artinya tidak memperdulikan huruf besar atau kecil.  kemudian masukkan nama file sebagai pencarian yaitu top_lagu_milik_farrel.csv.

selanjutnya kalian bisa menggabungkan hasil yang sudah kita check dan dimasukkan kedalam file playlist_keren.sh dengan cara :

```

(grep -i 'Hip Hop' top_lagu_milik_farrel.csv | sort -t, -k15,15nr | head -n 5 && \
grep -i 'John Mayer' top_lagu_milik_farrel.csv | sort -t, -k15,15n | head -n 5 && \
grep -i '2004' top_lagu_milik_farrel.csv | sort -t, -k15,15n | head -n 10 && \
grep -i 'sri' top_lagu_milik_farrel.csv) > playlist_keren.sh


```

<a href="https://ibb.co/JxW81DB"><img src="https://i.ibb.co/YD4JCHR/asdasdsa.png" alt="asdasdsa" border="0"></a>

dengan begitu hasil dari data yang sudah kita check maka akan masuk kedalam file tersebut dan semua tersimpan dengan baik.

## Pengerjaan Soal 1
<a href="https://ibb.co/Bg81Nb7"><img src="https://i.ibb.co/SKqLrb1/sisop1.png" alt="sisop1" border="0"></a>

# SOAL 2

Shinichi merupakan seorang detektif SMA yang kembali menjadi anak kecil karena ulah organisasi hitam. Dengan tubuhnya yang mengecil, Shinichi tidak dapat menggunakan identitas lamanya sehingga harus membuat identitas baru. Selain itu, ia juga harus membuat akun baru dengan identitas nya saat ini. Bantu Shinichi membuat program register dan login agar Shinichi dapat dengan mudah membuat semua akun baru yang ia butuhkan.

## Problem Soal 2

A. Shinichi akan melakukan register menggunakan email, username, serta password. Username yang dibuat bebas, namun email bersifat unique.

B. Shinichi khawatir suatu saat nanti Ran akan curiga padanya dan dapat mengetahui password yang ia buat. Maka dari itu, Shinichi ingin membuat password dengan tingkat keamanan yang tinggi.

- Password tersebut harus di encrypt menggunakan base64
- Password yang dibuat harus lebih dari 8 karakter
- Harus terdapat paling sedikit 1 huruf kapital dan 1 huruf kecil
- Password tidak boleh sama dengan username
- Harus terdapat paling sedikit 1 angka 
- Harus terdapat paling sedikit 1 simbol unik 

C. Karena Shinichi akan membuat banyak akun baru, ia berniat untuk menyimpan seluruh data register yang ia lakukan ke dalam folder users file users.txt. Di dalam file tersebut, terdapat catatan seluruh email, username, dan password yang telah ia buat.

D. Shinichi juga ingin program register yang ia buat akan memunculkan respon setiap kali ia melakukan register. Respon ini akan menampilkan apakah register yang dilakukan Shinichi berhasil atau gagal

E. Setelah melakukan register, Shinichi akan langsung melakukan login untuk memastikan bahwa ia telah berhasil membuat akun baru. Login hanya perlu dilakukan menggunakan email dan password.

F. Ketika login berhasil ataupun gagal, program akan memunculkan respon di mana respon tersebut harus mengandung username dari email yang telah didaftarkan.
- Ex: 
1. LOGIN SUCCESS - Welcome, [username]
2. LOGIN FAILED - email [email] not registered, please register first

G. Shinichi juga mencatat seluruh log ke dalam folder users file auth.log, baik login ataupun register.
- Format: [date] [type] [message]
- Type: REGISTER SUCCESS, REGISTER FAILED, LOGIN SUCCESS, LOGIN FAILED
- Ex: 
1. [23/09/17 13:18:02] [REGISTER SUCCESS] user [username] registered successfully
2. [23/09/17 13:22:41] [LOGIN FAILED] ERROR Failed login attempt on user with email [email]


## Solution Soal 2
[Source Code Soal 2](https://gitlab.com/subkhanmasudi8/sisop-praktikum-modul-1-2023-mh-it15/-/tree/main/soal_2?ref_type=heads)

### Solution Soal 2 A
```
#!/bin/bash

# Cek email
check_unique_email() {
    local email_to_check="$1"
    local users_file="users.txt"
    
    if grep -q "$email_to_check" "$users_file"; then
        echo "Email $email_to_check is already registered."
        return 1
    fi
    
    return 0
}

# Meminta user untuk menginputkan email dan uname
read -p "Enter your email: " email
read -p "Enter your username: " username

```
Langkah pertama adalah membuat file **register.sh**, kemudian kami membuat fungsi untuk mengecek keunikan email yang diregister oleh user dan mencatatnya dalam file **users.txt**. Kemudian fungsi ini akan melakukan pengecekan antara inputan email dari user dengan data email pada file **users.txt**. Jika ditemukan adanya kesamaan antara inputan email dengan data email pada file **users.txt**, maka akan muncul pesan **"Email is already registered"**. 

### Solution Soal 2 B
```
while true; do
    read -s -p "Enter your password: " password
    echo  

    if [ ${#password} -lt 8 ]; then
        echo "Password must be at least 8 characters long."
    elif ! echo "$password" | grep -q '[A-Z]' || ! echo "$password" | grep -q '[a-z]'; then
        echo "Password must contain at least 1 uppercase letter and 1 lowercase letter."
    elif ! echo "$password" | grep -q '[0-9]'; then
        echo "Password must contain at least 1 digit."
    elif ! echo "$password" | grep -q '[[:punct:]]'; then
        echo "Password must contain at least 1 special character."
    elif [ "$password" = "$username" ]; then
        echo "Password cannot be the same as the username."
    else
        break  
    fi
done

# Enkrip password
encrypted_password=$(echo -n "$password" | base64)


```
Kemudian program akan meminta inputan password dari user. Agar password yang diinput user tidak tampak di layar, kami menggunakan **read -s**. Setelah user menginputkan password, maka terdapat fungsi if else yang akan mengecek password apakah sesuai dengan requirements pada soal. 

1. **Pengecekan jumlah karakter password.** Pada kondisi ini, kami menggunakan **-lt 8** untuk mengecek apakah karakter password kurang dari 8. Jika kurang dari 8 karakter, maka akan muncul pesan **"Password must be at least 8 characters long"**.
2. **Pengecekan penggunaan uppercase dan lowercase letter.** Pada kondisi ini, kami menggunakan **grep -q** untuk mencari keberadaan huruf kapital A-Z dan huruf non-kapital a-z. Jika tidak ditemukan salah satu huruf kapital maupun non-kapital, maka akan muncul pesan **"Password must contain at least 1 uppercase letter and 1 lowercase letter"**.
3. **Pengecekan penggunaan digit angka.** Pada kondisi ini, kami menggunakan **grep -q** untuk mencari keberadaan digit angka 0-9. Jika tidak ditemukan salah satu digit angka, maka akan muncul pesan **"Password must contain at least 1 digit"**.
4. **Pengecekan penggunaan punctuation atau simbol unik (tanda baca).** Pada kondisi ini, kami menggunakan **grep -q** untuk mencari keberadaan simbol unik. Jika tidak ditemukan salah satu simbol unik, maka akan muncul pesan **"Password must contain at least 1 special character"**.
5. **Pengecekan kesamaan password dengan username.** Pada kondisi ini, kami membandingkan antara variabel username dan password. Jika ditemukan kesamaan password dengan username, maka akan muncul pesan **"Password cannot be the same as the username"**.
6. **Enkripsi password.** Kami mengenkripsi password dengan menggunakan base64.

### Solution Soal 2 C
```
if check_unique_email "$email"; then
    echo "$email:$username:$encrypted_password" >> "users.txt"

```
Pada kondisi tersebut, kami menyimpan variabel email, username dan password yang telah terenkripsi berdasrkan inputan user ke dalam file **users.txt**.

### Solution Soal 2 D
```
if check_unique_email "$email"; then
    echo "$email:$username:$encrypted_password" >> "users.txt"
    timestamp=$(date "+%d/%m/%y %H:%M:%S")  
    echo "[$timestamp] [REGISTER SUCCESS] user [$username] registered successfully" >> "auth.log"
    echo "Registration successful."
else
    echo "Registration failed."
    timestamp=$(date "+%d/%m/%y %H:%M:%S")   
    echo "[$timestamp] [REGISTER FAILED] Registration failed for email [$email]" >> "auth.log"
fi
```
Kode tersebut berfungsi untuk mengecek apakah user berhasil register atau tidak. Jika email yang diinputkan user belum terdeteksi sama dengan daftar email yang telah teregister pada file **users.txt**, maka akan muncul timestamp dan pesan bahwa user dengan username tertentu berhasil melakukan register, kemudian data tersebut akan tersimpan dalam file **auth.log**. Namun, jika email yang diinputkan user terdeteksi sama dengan dafatr email yang telah teregister pada file **users.txt**, maka akan muncul timestamp dan pesan bahwa user dengan username tertentu gagal melakukan register, kemudian data tersebut akan tersimpan dalam file **auth.log**.

### Solution Soal 2 E
```
read -p "Enter your email: " email
read -s -p "Enter your password: " password

```
Setelah berhasil melakukan register, user akan diminta untuk login dengan memasukkan email dan password yang telah diregister sebelumnya.

### Solution Soal 2 F & G
```
if check_email_exists "$email"; then
   
    user_data=$(grep "^$email:" "users.txt")

    IFS=':' read -r -a data <<< "$user_data"
    stored_password="${data[2]}"

    entered_password_encrypted=$(echo -n "$password" | base64)

    if [ "$entered_password_encrypted" = "$stored_password" ]; then
	timestamp=$(date "+%d/%m/%y %H:%M:%S")	
	echo "[$timestamp] [LOGIN SUCCESS] user [${data[1]}] logged in" >> "auth.log"
        echo "LOGIN SUCCESS - Welcome, [${data[1]}]"
    else
	timestamp=$(date "+%d/%m/%y %H:%M:%S")
 	echo "[$timestamp] [LOGIN FAILED] ERROR Failed login attempt on user with email $email" >> "auth.log"
 	echo "LOGIN FAILED - Incorrect password for email [$email]"
    fi
else
    timestamp=$(date "+%d/%m/%y %H:%M:%S")    
    echo "[$timestamp] [LOGIN FAILED] ERROR Failed login attempt on non-registered email $email" >> "auth.log"
    echo "LOGIN FAILED - Email [$email] not registered, please register first."
fi


```
Kode tersebut akan mengecek apakah password yang dimasukkan saat login sesuai dengan password yang telah diregister sebelumnya. Jika sesuai, maka akan muncul pesan **"LOGIN SUCCESS - Welcome, [username]"**. Jika tidak sesuai, maka akan muncul pesan **"LOGIN FAILED - email [email] not registered, please register first"**. Semua log percobaan register dan login yang dilakukan user akan tersimpan ke dalam file **auth.log** dengan tambahan adanya [timestamp] di setiap status percobaan register maupun login.

## Pengerjaan Soal 2
<a href="https://imgbb.com/"><img src="https://i.ibb.co/mF6s0fg/reg-suc.png" alt="reg-suc" border="0"></a> <br>
Screenshot di atas merupakan kondisi ketika user berhasil melakukan proses register.

<a href="https://imgbb.com/"><img src="https://i.ibb.co/wRNc9xv/reg-fail.png" alt="reg-fail" border="0"></a> <br>
Screenshot di atas merupakan kondisi ketika user tidak berhasil melakukan proses register.

<a href="https://imgbb.com/"><img src="https://i.ibb.co/m0w3yh6/req-pass.png" alt="req-pass" border="0"></a> <br>
Screenshot di atas merupakan kondisi ketika user memasukkan password untuk register tidak sesuai dengan requirements pada soal.

<a href="https://ibb.co/HxNkzvH"><img src="https://i.ibb.co/qW0PsGr/log-fail-reg-first.png" alt="log-fail-reg-first" border="0"></a> <br>
Screenshot di atas merupakan kondisi ketika user langsung mencoba login tanpa melakukan register sebelumnya.

<a href="https://imgbb.com/"><img src="https://i.ibb.co/wRNc9xv/reg-fail.png" alt="reg-fail" border="0"></a> <br>
Screenshot di atas merupakan kondisi ketika user salah memasukkan password sesuai dengan yang teregister sebelumnya.

<a href="https://imgbb.com/"><img src="https://i.ibb.co/6npX5pD/log-suc.png" alt="log-suc" border="0"></a> <br>
Screenshot di atas merupakan kondisi ketika user telah memasukkan password sesuai dengan yang teregister sebelumnya.

<a href="https://imgbb.com/"><img src="https://i.ibb.co/Hd1VhRd/users-txt.png" alt="users-txt" border="0"></a> <br>
Screenshot di atas merupakan isi dari file **users.txt** yang menyimpan variabel email, username dan password yang telah terenkripsi dengan base64.

<a href="https://ibb.co/bWt50HP"><img src="https://i.ibb.co/TvyMfKk/authlog.png" alt="authlog" border="0"></a> <br>
Screenshot di atas merupakan isi dari file **auth.log** yang menyimpan history log dari seluruh percobaan register dan login yang dilakukan oleh user.

## Kendala Soal 2
Pada soal nomor 2 ini, yang menjadi sedikit kendala kami adalah implementasi logika untuk skema requirements password yang disebutkan pada soal dan juga mengintegrasikan antara login dengan register.

# SOAL 3
Aether adalah seorang gamer yang sangat menyukai bermain game Genshin Impact. Karena hobinya, dia ingin mengoleksi foto-foto karakter Genshin Impact. Suatu saat Pierro memberikannya sebuah Tautan yang berisi koleksi kumpulan foto karakter dan sebuah clue yang mengarah ke endgame. Ternyata setiap nama file telah dienkripsi dengan menggunakan base64. Karena penasaran dengan apa yang dikatakan piero, Aether tidak menyerah dan mencoba untuk mengembalikan nama file tersebut kembali seperti semula.

## Problem Soal 3 A
Aether membuat script bernama genshin.sh, untuk melakukan unzip terhadap file yang telah diunduh dan decode setiap nama file yang terenkripsi dengan base64 . Karena pada file list_character.csv terdapat data lengkap karakter, Aether ingin merename setiap file berdasarkan file tersebut. Agar semakin rapi, Aether mengumpulkan setiap file ke dalam folder berdasarkan region tiap karakter
Format: Nama - Region - Elemen - Senjata.jpg

## Problem Soal 3 B
Karena tidak mengetahui jumlah pengguna dari tiap senjata yang ada di folder "genshin_character".Aether berniat untuk menghitung serta menampilkan jumlah pengguna untuk setiap senjata yang ada
Format: [Nama Senjata] : [total]
Untuk menghemat penyimpanan. Aether menghapus file - file yang tidak ia gunakan, yaitu genshin_character.zip, list_character.csv, dan genshin.zip

## Problem Soal 3 C
Namun sampai titik ini Aether masih belum menemukan clue endgame yang disinggung oleh Pierro. Dia berfikir keras untuk menemukan pesan tersembunyi tersebut. Aether membuat script baru bernama find_me.sh untuk melakukan pengecekan terhadap setiap file tiap 1 detik. Pengecekan dilakukan dengan cara meng-ekstrak tiap gambar dengan menggunakan command steghide. Dalam setiap gambar tersebut, terdapat sebuah file txt yang berisi string. Aether kemudian mulai melakukan dekripsi dengan base64 pada tiap file txt dan mendapatkan sebuah url. Setelah mendapatkan url yang ia cari, Aether akan langsung menghentikan program find_me.sh serta mendownload file berdasarkan url yang didapatkan.

## Problem Soal 3 D
Dalam prosesnya, setiap kali Aether melakukan ekstraksi dan ternyata hasil ekstraksi bukan yang ia inginkan, maka ia akan langsung menghapus file txt tersebut. Namun, jika itu merupakan file txt yang dicari, maka ia akan menyimpan hasil dekripsi-nya bukan hasil ekstraksi. Selain itu juga, Aether melakukan pencatatan log pada file image.log untuk setiap pengecekan gambar
Format: [date] [type] [image_path]
Ex: 
[23/09/11 17:57:51] [NOT FOUND] [image_path]
[23/09/11 17:57:52] [FOUND] [image_path]

## Problem Soal 3 E
Hasil akhir:
1. genshin_character
2. find_me.sh
3. genshin.sh
4. image.log
5. [filename].txt
6. [image].jpg

## Solution Soal 3
Membuat sebuah file baru bernama **genshin.sh** yang berisi
[Source Code Soal 3](https://gitlab.com/subkhanmasudi8/sisop-praktikum-modul-1-2023-mh-it15/-/tree/main/soal_3?ref_type=heads)

### Solution Soal 3 A

```
#!/bin/bash

zip_url="https://drive.google.com/file/d/1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2/view"

zip_file="genshin.zip"

decoded_filename=$(echo "$zip_file" | base64 -d)

wget "$zip_url" -O "$zip_file"

unzip "$zip_file" -d "$decoded_filename"

while IFS=',' read -r filename character region element weapon; do
    decoded_character=$(echo "$character" | base64 -d)
    decoded_region=$(echo "$region" | base64 -d)
    decoded_element=$(echo "$element" | base64 -d)
    decoded_weapon=$(echo "$weapon" | base64 -d)
    
    mkdir -p "$decoded_filename/$decoded_region"
    
    mv "$decoded_filename/$filename" "$decoded_filename/$decoded_region/$decoded_character - $decoded_region - $decoded_element - $decoded_weapon.jpg"
done < list_character.csv

```
Pertama, kami mengunduh sebuah tautan link yang berisi file **genshin.zip** dengan perintah **wget**. Kemudian file yang telah terunduh akan diekstrak ke dalam direktori dengan nama yang telah didekode. Kemudian program akan membaca baris-baris dari file **list_character.csv** dan menyimpannya ke dalam variabel-variabel yang sesuai. Kemudian program akan membuat direktori nama elemen yang telah didekode di dalam direktori yang telah diekstrak sebelumnya. Terakhir, program akan memindahkan file ke direktori yang sesuai dengan karakter, wilayah, elemen dan senjata karakter dengan format penamaan **Nama - Region - Elemen - Senjata.jpg**.

## Kendala Soal 3
Pada soal nomor 3 ini, yang menjadi kendala kami adalah pemahaman terhadap soal dan implementasi logika yang tepat sesuai dengan problem pada soal.

# SOAL 4

Defatra sangat tergila-gila dengan laptop legion nya. Suatu hari, laptop legion nya mendadak rusak 🙁 Tentu saja, Defatra adalah programer yang harus 24/7 ngoding tanpa menggunakan vscode di laptop legion nya.  Akhirnya, dia membawa laptop legion nya ke tukang servis untuk diperbaiki. Setelah selesai diperbaiki, ternyata biaya perbaikan sangat mahal sehingga dia harus menggunakan uang hasil slot nya untuk membayarnya. Menurut Kang Servis, masalahnya adalah pada laptop Defatra yang overload sehingga mengakibatkan crash pada laptop legion nya. Untuk mencegah masalah serupa di masa depan, Defatra meminta kamu untuk membuat program monitoring resource yang tersedia pada komputer.

## Problem Soal 4
Buatlah program monitoring resource pada laptop **Dengan Ketentuan**.

- Monitoring ram dan monitoring size suatu directory (Monitoring ram Digunakan command free -m, Monitoring disk Digunakan command du sh <target_path>)
- Catat semua metrics yang didapatkan dari hasil free -m.
- Catat Hasil size dari path directory du -sh <target_path> tersebut.
- Target_path yang akan dimonitor adalah /home/{user}/.

**A**. Masukkan semua metrics ke dalam suatu file log bernama **metrics_{YmdHms}.log**. **{YmdHms}** adalah waktu disaat file script bash kalian dijalankan. **Misal dijalankan pada 2023-01-31 15:00:00**, maka file log yang akan tergenerate **adalah metrics_20230131150000.log**.

**B**. Script untuk mencatat metrics diatas diharapkan dapat berjalan **otomatis pada setiap menit**.

**C**. Kemudian, buat satu script untuk **membuat agregasi file log ke satuan jam**. Script agregasi akan memiliki **info dari file-file yang tergenerate tiap menit**. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk **dijalankan setiap jam secara otomatis**. Berikut contoh nama file hasil agregasi metrics_agg_2023013115.log dengan format metrics_agg_{YmdH}.log

**D**. karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh **user pemilik file**.

### Solution soal 4A
[Source Code Soal 4](https://gitlab.com/subkhanmasudi8/sisop-praktikum-modul-1-2023-mh-it15/-/tree/main/soal_4?ref_type=heads)
```
#!/bin/bash

log_directory="/home/gilang/log"

timestamp=$(date "+%Y%m%d%H%M%S")
log_file=" ${log_directory}/metrics_${timestamp}.log"

ram_metrics=$(free -m | awk 'NR==2 {print $2","$3","$4","$5","$6","$7}')

target_path="/home/gilang/"
disk_metrics=$(du -sh "$target_path" | awk '{print $1}')

echo "$ram_metrics,$swap_metrics,$target_path,$disk_metrics" > $log_file
```

Langkah Pertama dalam pengerjaan soal nomer 4 adalah membaut script dahulu yang bernama minuute_log.sh. lalu kita melakukan edit script tersebut dengan menggunakan text editor yang bernama NANO. pada line command pertama kita menemtukan dimana kita akan menyimpan file log permenit, ```log_directory="/home/gilang/log```, disini kita menggunakan log_directory untuk menentukan dimana kita mau menyimpan log hasil metrics permenit.
#### line ke 2 
```
timestamp=$(date "+%Y%m%d%H%M%S")
```
Berfungsi mengambil waktu saat ini dalam format tahun bulan tanggal jam menit detik dan menyimpannya dalam variabel timestamp.
#### Line ke 3 
```
log_file=" ${log_directory}/metrics_${timestamp}.log"
```
Berfungsi Membuat nama file log dengan menambahkan timestamp tadi ke dalam direktori log yang telah didefinisikan sebelumnya.
#### line Ke 4 
```
ram_metrics=$(free -m | awk 'NR==2 {print $2","$3","$4","$5","$6","$7}')
```
Berfungsi mengambil penggunaan RAM  menggunakan perintah ( free -m ), kemudian mengolah hasilnya dengan command ( AWK ) dan mengambil nilai yang diperlukan dan menyimpannya dalam command ( ram_metrics ).
#### Line Ke 5 
```
target_path="/home/gilang/"
```
Berfingsi  mendefinisikan target path di ( **/home/gilang/** )
#### Line ke 6  
```
disk_metrics=$(du -sh "$target_path" | awk '{print $1}')
```
Berfungsi mengambil metrik penggunaan disk di target path yang telah ditentukan dengan command ( **du -sh** ), kemudian mengambil nilai ukuran disk kemudian menyimpannya dalam command ( **disk_metrics** ).
#### Line Ke 7 
```
echo "$ram_metrics,$swap_metrics,$target_path,$disk_metrics" > $log_file
```
Berfungsi menggabungkan semua metrik yang telah di  buat tadi(**RAM, target path, dan disk**) menjadi satu dalam sebuah string dan menyimpannya dalam file log yang telah dibuat sebelumnya.

### Solution soal 4B
Karena scipr minute_log.sh Harus diajalankan **setiap menit** maka perlu menambahkan **Crontab** seperti berikut ```* * * * * /home/gilang/soal_4/minute_log.sh``` Pada cromjob tersebut terdapat **5 Bintang yang merepresentasikan menit**.

### Solution Soal 4C
Selanjutnya karena semua file log permenit yang teregerate tadi **perlu digabungkan menjadi satu**, maka perlu membuat script baru yang fungsinya menggabungkan info dari file-file yang tergenerate tiap menit tadi. Dalam **hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics**. contoh file **agregasi = metrics_agg_2023013115.log** dengan **format metrics_agg_{YmdH}.log** 
```
log_dir="/home/gilang/log"

hourly_timestamp=$(date +'%Y%m%d%H')

cat "$log_dir/metrics_$hourly_timestamp"* > "$log_dir/metrics_agg_$hourly_timestamp.log"

min_ram=$(awk -F',' 'NR>1{print $2}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -n | head -n 1)
max_ram=$(awk -F',' 'NR>1{print $2}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -n | tail -n 1)
avg_ram=$(awk -F',' 'NR>1{sum+=$2}END{print sum/(NR-1)}' "$log_dir/metrics_agg_$hourly_timestamp.log")

min_disk=$(awk -F',' 'NR>1{print $12}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -h | head -n 1)
max_disk=$(awk -F',' 'NR>1{print $12}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -h | tail -n 1)
avg_disk=$(awk -F',' 'NR>1{sum+=$12}END{print sum/(NR-1)}' "$log_dir/metrics_agg_$hourly_timestamp.log")

temp_file="$log_dir/metrics_agg_temp_$hourly_timestamp.log"

echo "minimum,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$log_dir" > "$temp_file"
echo "maximum,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$log_dir" >> "$temp_file"
echo "average,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$log_dir" >> "$temp_file"

cat "$temp_file" >> "$log_dir/metrics_agg_$hourly_timestamp.log"

rm "$temp_file"

```
Code tersebut digunakan untuk **menampilan agrerat per jem** dari kombinase kode yang di jalankan otomatis oleh minute_log.sh. code ```log_dir="/home/gilang/log"``` menunjukan tempat directory metrics disimpan. 
#### Line ke 2 dan 3
```
hourly_timestamp=$(date +'%Y%m%d%H') 

cat "$log_dir/metrics_$hourly_timestamp"* > "$log_dir/metrics_agg_$hourly_timestamp.log"
```
menunjukan format timestamp disimpan, disini berupa tahun, bulan, tanggal, dan jam. **Timestamp ini akan digunakan untuk menandai agregasi data log per jam**. Sedangkan **lines ke 3** berfungsi menggabungkan semua file log per menit yang sesuai dengan timestamp saat ini menjadi satu file log agregat dengan nama ```"metrics_agg_<timestamp>.log"```
#### Line Ke 4 - 9
```
min_ram=$(awk -F',' 'NR>1{print $2}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -n | head -n 1)
max_ram=$(awk -F',' 'NR>1{print $2}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -n | tail -n 1)
avg_ram=$(awk -F',' 'NR>1{sum+=$2}END{print sum/(NR-1)}' "$log_dir/metrics_agg_$hourly_timestamp.log")
min_disk=$(awk -F',' 'NR>1{print $12}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -h | head -n 1)
max_disk=$(awk -F',' 'NR>1{print $12}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -h | tail -n 1)
avg_disk=$(awk -F',' 'NR>1{sum+=$12}END{print sum/(NR-1)}' "$log_dir/metrics_agg_$hourly_timestamp.log")
```
Berfungsi sebagai perintah-perintah yang digunakan untuk **menghitung nilai minimum, maksimum, dan rata-rata dari metrik RAM** (Lines 4-6) dan **Disk** (Lines 5-9) dalam file log agregat yang telah dibuat sebelumnya.
#### Line ke 5
```
temp_file="$log_dir/metrics_agg_temp_$hourly_timestamp.log"
```
Berfungsi **menyimpan  file sementara** yang akan digunakan untuk menyimpan code hasil agregasi.
#### Line ke 6 - 8
```
echo "minimum,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$log_dir" > "$temp_file"
echo "maximum,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$log_dir" >> "$temp_file"
echo "average,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$log_dir" >> "$temp_file"
```
Berfungsi code yang digunakan untuk membuat baris-baris yang berisi **hasil agregasi (minimum, maksimum, dan rata-rata) untuk metrik RAM, Disk dan Path Location**. Data ini akan disimpan dalam file sementara.
#### Line ke 9
```
cat "$temp_file" >> "$log_dir/metrics_agg_$hourly_timestamp.log"
```
Berfungsi menggabungkan isi file sementara ke dalam file log agregat akhir.
#### Line ke 10
```
rm "$temp_file"
```
Berfungsi menghapus file sementara setelah data agregat telah ditambahkan ke file log akhir.

### Solution Soal 4D
Dikarenakan file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file. Maka cara penyelesaian dengan menambahkan ```chmod 600 /home/gilang/log```, script tersebut berfungsi untuk membuat folder log hanya dapat diakses oleh **Default** user.

## Pengerjaan Soal 4
<a href="https://ibb.co/5Tv4TfS"><img src="https://i.ibb.co/wyCgyxP/prak-1-gmbr-1.png" alt="prak-1-gmbr-1" border="0"></a>
Screenshot di atas merupakan kode minuute_log.sh.

<a href="https://imgbb.com/"><img src="https://i.ibb.co/1bKLxb3/prak-1-gmbr-2.png" alt="prak-1-gmbr-2" border="0"></a>
Screenshot di atas merupakan kode minuute_log.sh yang tereksekusi otomatis setelab 1 menit, dengan menggunakan command Crontab. 

<a href="https://imgbb.com/"><img src="https://i.ibb.co/k3McYXN/prak-1-gmbr-3.png" alt="prak-1-gmbr-3" border="0"></a> 
**NB. code yang atas**

<a href="https://ibb.co/S779FFr"><img src="https://i.ibb.co/Kzz3TTw/prak-1-gmbr-4.png" alt="prak-1-gmbr-4" border="0"></a> Kode tersebut berfungsi sebagai agregasi kode minuute_log.sh setelah 1 jam. 

<a href="https://ibb.co/zSdYcZF"><img src="https://i.ibb.co/cNqH5Qw/prak-1-gmbr-5.png" alt="prak-1-gmbr-5" border="0"></a> Contoh file log nya yang keluar setelah 1 jam.

<a href="https://imgbb.com/"><img src="https://i.ibb.co/k3McYXN/prak-1-gmbr-3.png" alt="prak-1-gmbr-3" border="0"></a> 
Diatas adalah Crontab yang digunakan untuk membuat file agregasi berjalan setiap 1 jam **NB yang bawah**

<a href="https://imgbb.com/"><img src="https://i.ibb.co/0rbf5Z1/prak-1-gmbr-6.png" alt="prak-1-gmbr-6" border="0"></a> 
Code ini dugunakan suapaya folder log tempat menyimpan metrics dan agregasinya hanya dapat diakses oleh default user

## Revisi

#### Crontab

**Crontab** 0 * * * * /home/gilang/soal_4/aggregate_minutes_to_hourly_log_sh Salah Di ganti menjadi **Crontab** ```59 * * * * /home/gilang/soal_4/aggregate_minutes_to_hourly_log_sh```

#### Aggregate_minutes_to_hourly_log.sh

Pada aggregate_minutes_to_hourly_log.sh belum ada Target_path nya
```
echo "minimum,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_disk" >> "$temp_file"
echo "maximum,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_disk" >> "$temp_file"
echo "average,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_disk" >> "$temp_file"
```

Dengan menambahkan ```$log_dir``` di setiap ```$echo``` maka target path akan muncul di file log
```
log_dir="/home/gilang/log"

hourly_timestamp=$(date +'%Y%m%d%H')

cat "$log_dir/metrics_$hourly_timestamp"* > "$log_dir/metrics_agg_$hourly_timestamp.log"

min_ram=$(awk -F',' 'NR>1{print $2}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -n | head -n 1)
max_ram=$(awk -F',' 'NR>1{print $2}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -n | tail -n 1)
avg_ram=$(awk -F',' 'NR>1{sum+=$2}END{print sum/(NR-1)}' "$log_dir/metrics_agg_$hourly_timestamp.log")

min_disk=$(awk -F',' 'NR>1{print $12}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -h | head -n 1)
max_disk=$(awk -F',' 'NR>1{print $12}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -h | tail -n 1)
avg_disk=$(awk -F',' 'NR>1{sum+=$12}END{print sum/(NR-1)}' "$log_dir/metrics_agg_$hourly_timestamp.log")

temp_file="$log_dir/metrics_agg_temp_$hourly_timestamp.log"

echo "minimum,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$log_dir" > "$temp_file"
echo "maximum,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$log_dir" >> "$temp_file"
echo "average,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$log_dir" >> "$temp_file"

cat "$temp_file" >> "$log_dir/metrics_agg_$hourly_timestamp.log"

rm "$temp_file"

```
## Problem


- Swap_metrics pada minuute_log.sh dan aggregate_minutes_to_hourly_log.sh tidak muncul.
- Terdapat Kesalahan Pada aggregate_minutes_to_hourly_log.sh sehingga hanya menghitung max, min, avg ram dan disk.

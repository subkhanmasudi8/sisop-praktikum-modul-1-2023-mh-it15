#!/bin/bash

# Cek email
check_unique_email() {
    local email_to_check="$1"
    local users_file="users.txt"
    
    if grep -q "$email_to_check" "$users_file"; then
        echo "Email $email_to_check is already registered."
        return 1
    fi
    
    return 0
}

# Meminta user untuk menginputkan email dan uname
read -p "Enter your email: " email
read -p "Enter your username: " username

# Meminta input password dari user dengan "read -s" untuk menyembunyikan input
while true; do
    read -s -p "Enter your password: " password
    echo  # Memberi space kosong sebaris

    # Memeriksa panjang password
    if [ ${#password} -lt 8 ]; then
        echo "Password must be at least 8 characters long."
    elif ! echo "$password" | grep -q '[A-Z]' || ! echo "$password" | grep -q '[a-z]'; then
        echo "Password must contain at least 1 uppercase letter and 1 lowercase letter."
    elif ! echo "$password" | grep -q '[0-9]'; then
        echo "Password must contain at least 1 digit."
    elif ! echo "$password" | grep -q '[[:punct:]]'; then
        echo "Password must contain at least 1 special character."
    elif [ "$password" = "$username" ]; then
        echo "Password cannot be the same as the username."
    else
        break  # Keluar dari loop jika semua persyaratan terpenuhi
    fi
done

# Enkrip password
encrypted_password=$(echo -n "$password" | base64)

# Cek email
if check_unique_email "$email"; then
    # Menyimpan data user yang telah dienkripsi ke dalam file users.txt
    echo "$email:$username:$encrypted_password" >> "users.txt"
    timestamp=$(date "+%d/%m/%y %H:%M:%S")  
    echo "[$timestamp] [REGISTER SUCCESS] user [$username] registered successfully" >> "auth.log"
    echo "Registration successful."
else
    echo "Registration failed."
    timestamp=$(date "+%d/%m/%y %H:%M:%S")   
    echo "[$timestamp] [REGISTER FAILED] Registration failed for email [$email]" >> "auth.log"
fi






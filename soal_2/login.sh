#!/bin/bash

# Cek email dalam file users.txt
check_email_exists() {
    local email_to_check="$1"
    local users_file="users.txt"
    
    if grep -q "^$email_to_check:" "$users_file"; then
        return 0  # ada email
    else
        return 1  # tidak ada email
    fi
}

# Minta inputan dari user
read -p "Enter your email: " email
read -s -p "Enter your password: " password
echo # Memberi space sebaris

# Cek keberadaan email dalam file users.txt
if check_email_exists "$email"; then
    # Mengambil baris yang sesuai dengan email dari file users.txt
    user_data=$(grep "^$email:" "users.txt")
    # Memisahkan baris data menjadi variabel email, username dan encrypted_password
    IFS=':' read -r -a data <<< "$user_data"
    stored_password="${data[2]}"

    # Enkrip password
    entered_password_encrypted=$(echo -n "$password" | base64)

    # Cek inputan password apakah sesuai dengan yang terenkripsi dalam file users.txt
    if [ "$entered_password_encrypted" = "$stored_password" ]; then
	timestamp=$(date "+%d/%m/%y %H:%M:%S")	
	echo "[$timestamp] [LOGIN SUCCESS] user [${data[1]}] logged in" >> "auth.log"
        echo "LOGIN SUCCESS - Welcome, [${data[1]}]"
    else
	timestamp=$(date "+%d/%m/%y %H:%M:%S")
 	echo "[$timestamp] [LOGIN FAILED] ERROR Failed login attempt on user with email $email" >> "auth.log"
 	echo "LOGIN FAILED - Incorrect password for email [$email]"
    fi
else
    timestamp=$(date "+%d/%m/%y %H:%M:%S")    
    echo "[$timestamp] [LOGIN FAILED] ERROR Failed login attempt on non-registered email $email" >> "auth.log"
    echo "LOGIN FAILED - Email [$email] not registered, please register first."
fi


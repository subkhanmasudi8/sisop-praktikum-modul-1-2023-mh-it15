#!/bin/bash

zip_url="https://drive.google.com/file/d/1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2/view"

zip_file="genshin.zip"

decoded_filename=$(echo "$zip_file" | base64 -d)

wget "$zip_url" -O "$zip_file"

unzip "$zip_file" -d "$decoded_filename"

while IFS=',' read -r filename character region element weapon; do
    # Dekripsi nama file
    decoded_character=$(echo "$character" | base64 -d)
    decoded_region=$(echo "$region" | base64 -d)
    decoded_element=$(echo "$element" | base64 -d)
    decoded_weapon=$(echo "$weapon" | base64 -d)
    
    # Folder region
    mkdir -p "$decoded_filename/$decoded_region"
    
    # Memindahkan file ke folder berdasarkan region dan format nama sesuai soal
    mv "$decoded_filename/$filename" "$decoded_filename/$decoded_region/$decoded_character - $decoded_region - $decoded_element - $decoded_weapon.jpg"
done < list_character.csv


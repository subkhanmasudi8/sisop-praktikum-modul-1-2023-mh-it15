#!/bin/bash

log_directory="/home/gilang/log"

timestamp=$(date "+%Y%m%d%H%M%S")
log_file=" ${log_directory}/metrics_${timestamp}.log"

ram_metrics=$(free -m | awk 'NR==2 {print $2","$3","$4","$5","$6","$7}')
#swap_metrics=$(free -m | awk 'NR=3 {print $2","$3","$4}')

target_path="/home/gilang/"
disk_metrics=$(du -sh "$target_path" | awk '{print $1}')

echo "$ram_metrics,$swap_metrics,$target_path,$disk_metrics" > $log_file

# Command crontab
* * * * * /home/gilang/soal_4/minute_log.sh

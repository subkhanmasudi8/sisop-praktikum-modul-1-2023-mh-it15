#!/bin/bash

log_dir="/home/gilang/log"

hourly_timestamp=$(date +'%Y%m%d%H')

cat "$log_dir/metrics_$hourly_timestamp"* > "$log_dir/metrics_agg_$hourly_timestamp.log"

min_ram=$(awk -F',' 'NR>1{print $2}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -n | head -n 1)
max_ram=$(awk -F',' 'NR>1{print $2}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -n | tail -n 1)
avg_ram=$(awk -F',' 'NR>1{sum+=$2}END{print sum/(NR-1)}' "$log_dir/metrics_agg_$hourly_timestamp.log")

min_disk=$(awk -F',' 'NR>1{print $12}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -h | head -n 1)
max_disk=$(awk -F',' 'NR>1{print $12}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -h | tail -n 1)
avg_disk=$(awk -F',' 'NR>1{sum+=$12}END{print sum/(NR-1)}' "$log_dir/metrics_agg_$hourly_timestamp.log")

temp_file="$log_dir/metrics_agg_temp_$hourly_timestamp.log"

echo "minimum,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_disk" >> "$temp_file"
echo "maximum,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_disk" >> "$temp_file"
echo "average,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_disk" >> "$temp_file"

cat "$temp_file" >> "$log_dir/metrics_agg_$hourly_timestamp.log"

rm "$temp_file"

# Command crontab
0 * * * * /home/gilang/soal_4/aggregate_minutes_to_hourly_log.sh

# Revisi 
log_dir="/home/gilang/log"

hourly_timestamp=$(date +'%Y%m%d%H')

cat "$log_dir/metrics_$hourly_timestamp"* > "$log_dir/metrics_agg_$hourly_timestamp.log"

min_ram=$(awk -F',' 'NR>1{print $2}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -n | head -n 1)
max_ram=$(awk -F',' 'NR>1{print $2}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -n | tail -n 1)
avg_ram=$(awk -F',' 'NR>1{sum+=$2}END{print sum/(NR-1)}' "$log_dir/metrics_agg_$hourly_timestamp.log")

min_disk=$(awk -F',' 'NR>1{print $12}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -h | head -n 1)
max_disk=$(awk -F',' 'NR>1{print $12}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -h | tail -n 1)
avg_disk=$(awk -F',' 'NR>1{sum+=$12}END{print sum/(NR-1)}' "$log_dir/metrics_agg_$hourly_timestamp.log")

temp_file="$log_dir/metrics_agg_temp_$hourly_timestamp.log"

echo "minimum,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$log_dir" > "$temp_file"
echo "maximum,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$log_dir" >> "$temp_file"
echo "average,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$log_dir" >> "$temp_file"

cat "$temp_file" >> "$log_dir/metrics_agg_$hourly_timestamp.log"

rm "$temp_file"
